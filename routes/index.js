const express = require('express');
const router = new express.Router();

//sort
function sort(obj) {
    return Object.keys(obj).sort(function(a, b) {
      return obj[b] - obj[a];
    });
  }
  
  
  //root
  router.get('/', (req, res) => {
      res.redirect('/sort');
  });
  
  
  //sort
  router.get('/sort', (req, res)  => {
      res.render('index');
  });
  
  
  
  router.post('/sort', (req, res) => {
  
  
      const response_arr = req.body.arr;


  
      let arr = response_arr.split(',').map(function(item) {
          return parseInt(item, 10);
      });
      
    

      let errval = false;

      let i = 0; 
      for (; i < arr.length; i++){

        // check if array value is false or NaN
        if ( Number.isNaN(arr[i]) ) {
            errval =true;
        }
    
      }

      //logic starts here
      var frequency_map = {};

      for (i=0; i < arr.length; i++) {
        let num = arr[i];
        frequency_map[num] = frequency_map[num] ? frequency_map[num] + 1 : 1;
      }
  
  
      let sorted_key = sort(frequency_map);
      let sorted_val = sorted_key.map(function(key) { 
          return frequency_map[key];
      });
      let output_set = [];
      for(i = 0; i<sorted_key.length; i++ ){
          let inputstr = sorted_key[i] + ' is ' + sorted_val[i];
          let timeappend = sorted_val[i] < 2 ? ' time': ' times';
          output_set.push(inputstr+timeappend);
      }
  
      if(errval === true){
        res.render('index', {errval : errval});
      }
      else{
        res.render('output', {output_set:output_set});    
      }
  });
  


module.exports = router;